<?php
require_once 'aws.phar';
require_once 'Monitor.php';

use MonitorAWS\Monitor;

try {
    $monitor = new Monitor();
    $monitor->watch();
} catch (Exception $e){
    echo "Error fatal: $e\n";
}