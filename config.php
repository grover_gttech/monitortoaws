<?php
/**
 * Debe devolver la configuración para el monitor
 * @return array
 */
return array(
  /* string, es el nombre que saldrá listado en las métricas de CloudWatch */
  'namespace' => 'GtTech',
  /* array, configura el cliente del servicio */   
  'client'    => array(
    'key'    => 'xxxxxxxxxxxxxxxxxxxx',
    'secret' => 'xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx',
    'region' => 'us-east-1'
  ),
  /* array, la lista de observadores */
  'observers' => array(
	'ApacheSubstring' => array(
	  'metric' => 'Apache',
	  'url'    => 'http://localhost/',
	  'needle' => '1'
    ),
    /*
     * Ejemplo para otro recurso que usa el mismo driver
    1 => array(
      'driver' => 'ApacheSubstring',
	  'metric' => 'Apache2',
	  'url'    => 'http://otro.host.com/',
	  'needle' => '1'
    ),    
    */
    'MySQLi' => array(
      'metric'   => 'MySQL',
      'dbConfig' => array(
        'host'     => 'localhost',
        'username' => 'gttechmonitor',
        'password' => 'xxxxxxxxxxx',
        'socket'   => '/var/run/mysql/mysql.sock'
      ),
	),
    'MongoDb' => array(
    	'metric' => 'MongoDb'
    ),
    'DiskFree' => array(
        'metric' => 'Disco Libre',
        'directorio' => '/'
    ),
    'DiskUsage' => array(
        'metric' => 'Disk Usage',
    ),
    'MemoryUsage' => array(
        'metric' => 'Memory Usage'
    ),
    2 => array(
        'driver' => 'Memory',
        'metric' => 'Memoria libre',
        'type'   => 1
    ),
    3 => array(
        'driver' => 'Memory',
        'metric' => 'Memoria usada',
        'type'   => 2
    ),
    4 => array(
        'driver' => 'Memory',
        'metric' => 'Memoria total',
        'type'   => 3
    )
  ),
  /* ¿Se desea guardar en un log las actividades? */
  'debug' => false 
);
