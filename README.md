# MonitorToAWS #
-----------

Paquete liberado por [GT Tech](http://www.gttech.pe) que permite configurar rápidamente diversos recursos para ser monitoreados usando CloudWatch de Amazon AWS. Está diseñado de tal manera que cada recurso a ser monitoreado sea en una arquitectura de plugin. En su primera versión se ofrece algunos plugins para monitorear: MySQL, MongoDb, Apache, Apache extrayendo una cadena de texto y Espacio libre de un disco. Es fácil de configurar y permitirá tener alarmas para estos recursos que fueron olvidados en las métricas de Amazon CloudWatch.

## Características ##
---------------

* Fácil de configurar.
* Permite monitorear varios recursos del mismo tipo.
* Está construído usando PHP 5.4 y superior.
* Utiliza el SDK en PHP de Amazon AWS en formato phar, a menos que exista una actualización que no sea compatible hacia atrás debería ser posible su actualización.
* Se invita a la comunidad a crear más drivers para otros recursos.

## Configuración ##
-------------

* Configurar archivo config.php para incluir los parámetros de conexión a AWS y los observadores que se desean, ejemplo:


```
#!php

<?php
/**
 * Debe devolver la configuración para el monitor
 * @return array
 */
return array(
  /* string, es el nombre que saldrá listado en las métricas de CloudWatch */
  'namespace' => 'GtTech',
  /* array, configura el cliente del servicio */   
  'client'    => array(
    'key'    => 'xxxxxxxxxxxxxxxxxxxx',
    'secret' => 'xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx',
    'region' => 'us-east-1'
  ),
  /* array, la lista de observadores */
  'observers' => array(
    'ApacheSubstring' => array(
      'metric' => 'Apache',
      'url'    => 'http://localhost/',
      'needle' => '1'
    ),
    /*
     * Ejemplo para otro recurso que usa el mismo driver
    1 => array(
      'driver' => 'ApacheSubstring',
      'metric' => 'Apache2',
      'url'    => 'http://otro.host.com/',
      'needle' => '1'
    ),    
    */
    'MySQLi' => array(
      'metric'   => 'MySQL',
      'dbConfig' => array(
        'host'     => 'localhost',
        'username' => 'gttechmonitor',
        'password' => 'xxxxxxxxxxx',
        'socket'   => '/var/run/mysql/mysql.sock'
      ),
    ),
    'MongoDb' => array(
        'metric' => 'MongoDb'
    ),
    'DiskFree' => array(
        'metric' => 'DiscoLibre',
        'directorio' => '/'
    ),
    'DiskUsage' => array(
        'metric' => 'Disk Usage',
    ),
    'MemoryUsage' => array(
        'metric' => 'Memory Usage'
    ),
    2 => array(
        'driver' => 'Memory',
        'metric' => 'Memoria libre',
        'type'   => 1
    ),
    3 => array(
        'driver' => 'Memory',
        'metric' => 'Memoria usada',
        'type'   => 2
    ),
    4 => array(
        'driver' => 'Memory',
        'metric' => 'Memoria total',
        'type'   => 3
    )
  ),
  /* ¿Se desea guardar en un log las actividades? */
  'debug' => false 
);
```

* Guardar todo el paquete en un directorio del servidor y añadirlo al cron
```
#!bash

 * * * * * /usr/bin/php -f $HOME/MonitorToAWS/index.php > /dev/null 2>&1
```

* Esperar unos minutos para que se empiece a coleccionar datos.
* Entrar a la consola de AWS / CloudWatch y revisar datos.
* Crear alarmas en caso necesario.

## Drivers ##
---------

La primera versión de MonitorToAWS incluye algunos drivers que se crearon bajo la necesidad. Se hace la invitación para que todos puedan aportar con sus drivers. 

### ¿Cómo crear nuevos drivers? ###

1. Para crear un nuevo driver se debe crear un archivo con el nombre del driver, ejemplo: DriskFree.php.
2. La clase debe pertenecer al namespace Drivers.
3. Dentro de él debe existir una clase con el mismo nombre que extiende de \DriverAbstract.
4. La clase debe tener un valor para el nombre de la métrica a través del atributo protegido $_metric. Sin embargo, es posible sobreescribir este nombre de métrica desde el archivo de configuración, esto es útil cuando se necesita tener varias mediciones del mismo driver.
5. La clase debe implementar el método abstracto watch, que es llamado cuando deba hacer una medición.
6. Dentro del método watch debe usar el método "publicar" para enviar una medición al CloudWatch.

Ejemplo de driver:

```
#!php

<?php
namespace MonitorAWS\Drivers;
use MonitorAWS\Drivers\DriverAbstract;

/**
 * Driver para monitorear el espacio libre en una partición
 *
 * @author Grover Campos Ancajima <grover@gttech.pe>
 * @version 0.1
 * @copyright GT Tech <http://www.gttech.pe>
 */
class DiskFree extends DriverAbstract
{
    /**
     * Nombre de la métrica, debe ser implementado en cada clase de un driver
     * 
     * @var string
     */
    protected $_metric = 'DiskFree';

    /**
     * Variable de trabajo del driver.
     * Directorio a consultar el espacio libre.
     * 
     * @var string
     */
    private $_directorio = '/';
    
    /**
     */
    public function __construct(array $config) {
        parent::__construct($config);
        if (!empty($config['directorio'])) {
            $this->_directorio = $config['directorio'];
        }
    }
    
    /**
     * (non-PHPdoc)
     *
     * @see DriverAbstract::watch()
     *
     */
    public function watch() {
        $libre = disk_free_space($this->_directorio) / (1024^2);
        if ($libre) {
            $this->publicar(ceil($libre), 'Megabytes');
        } else {
            $this->error('Falló al buscar el espacio libre');
        }
    }
}
```

### Apache ###

Este driver verifica que una página conteste y la conexión devuelva un código HTTP 200, en ese caso envía un valor 1 al CloudWatch, cualquier otro código HTTP enviará un valor 0. La idea es poder crear un monitor para que verifique que el apache esté vivo, y permite crear una alarma para cuando el valor de esta métrica llegue a 0.

Es requisito para usar este driver la extensión cURL de PHP.

Para configurarlo se debe establecer un key llamado url en la configuración, ejemplo:
 
```
#!php

<?php
/**
 * Debe devolver la configuración para el monitor
 * @return array
 */
return array(
  ...
  'observers' => array(
    /* driver apache */
    'Apache' => array(
      'metric' => 'Apache',
      'url'    => 'http://localhost/'
    ),
    ...
  )
  ... 
);
```

### ApacheSubstring ###

Extiende la funcionalidad del driver Apache y no solo verifica que el servidor responda, sino que también verifica que el html contenga una cadena especificada. Esto es útil, por ejemplo, para hacer comprobaciones del correcto funcionamiento de algunos servicios. 

La configuración es similar a la del driver Apache, pero incluye otro key llamado needle que es el string a buscar en el html. Ejemplo:
 
```
#!php

<?php
/**
 * Debe devolver la configuración para el monitor
 * @return array
 */
return array(
  ...
  'observers' => array(
    /* driver apache */
    'ApacheSubstring' => array(
      'metric' => 'Apache',
      'url'    => 'http://localhost/',
      'needle' => 'OK'
    ),
    ...
  )
  ... 
);
```

### DiskFree ###

Este driver consulta el espacio libre disponible en una partición, especificada a través del directorio donde está montado. El cálculo se basa en el uso de la función [disk_free_space](http://php.net/manual/en/function.disk-free-space.php), así que el argumento podría ser usado tanto para linux como para windows según expone el manual.

La configuración sólo necesita de un key llamado "directorio". Ejemplo:

```
#!php

<?php
/**
 * Debe devolver la configuración para el monitor
 * @return array
 */
return array(
  ...
  'observers' => array(
    ...
    'DiskFree' => array(
        'metric' => 'DiscoLibre',
        'directorio' => '/'
    )
  ),
  ... 
);
```

### MongoDb ###

Driver que permite contabilizar las conexiones existentes en el servidor de MongoDb. Por ahora asume que el servidor se encuentra en el localhost y tiene activa la [consola HTTP](http://docs.mongodb.org/ecosystem/tools/http-interfaces/#http-console).

No existe requisitos de extensiones para usar este driver, necesita que se configure y active la consola HTTP.

La configuración es opcional, los valores son host y port. Ejemplo:

```
#!php

<?php
/**
 * Debe devolver la configuración para el monitor
 * @return array
 */
return array(
  ...
  'observers' => array(
    ...
    'MongoDb' => array(
        'metric' => 'MongoDb',
        'host' => '127.0.0.1',
        'port' => 28017
    ),
    ...
  ),
  ... 
);
```

### MySQLi ###

Driver que permite verificar que esté vivo un servidor MySQL. Enviará un 1 si el servidor responde, y un 0 si no es así.

Este driver necesita la extensión mysqli.

Para su configuración se requiere un key llamado dbConfig, que es un array asociativo conteniendo los parámetros para la conexión al servidor MySQL. Ejemplo: 

```
#!php

<?php
/**
 * Debe devolver la configuración para el monitor
 * @return array
 */
return array(
  ...
  'observers' => array(
    ...
    'MySQLi' => array(
      'metric'   => 'MySQL',
      'dbConfig' => array(
        'host'     => 'localhost',
        'username' => 'gttechmonitor',
        'password' => 'xxxxxxxxxxx',
        'socket'   => '/var/run/mysql/mysql.sock'
      ),
    ),
    ...
  ),
  ... 
);
```

### Memory ###

Driver que permite monitorear la memoria. Permite configurar para monitorear la memoria total, libre y la usada. Usa la función [exec](http://php.net/manual/en/function.exec.php) por lo que se requiere que esta función esté apta para ejecutarse.

Para configurar este driver se necesita configurar el key "type" con valores del 1 al 3 que indican: 

1. Memoria libre
2. Memoria usada
3. Memoria total

Ejemplo:

```
#!php

<?php
/**
 * Debe devolver la configuración para el monitor
 * @return array
 */
return array(
  ...
  'observers' => array(
    ...
    1 => array(
      'driver' => 'Memory',
      'metric' => 'Memoria libre',
      'type' => 1  # Memoria libre
    ),
    2 => array(
      'driver' => 'Memory',
      'metric' => 'Memoria total',
      'type' => 3  # Memoria total
    )
    ...
  ),
  ... 
);
```
