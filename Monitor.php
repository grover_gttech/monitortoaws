<?php
namespace MonitorAWS;

require_once 'MonitorException.php';
require_once 'Drivers/Abstract.php';

/**
 * Monitor de recursos usando AWS CloudWatch 
 * 
 * @author Grover Campos <grover@gttech.pe>
 * @version Mar 31, 2014
 * @copyright GT Tech <http://www.gttech.pe>
 */
class Monitor
{
    /**
     * Observadores cargados
     * 
     * @var array
     */
    private $_observers;
    /**
     * Constructor y recuperador
     */
    public function __construct() {
        $config = require_once 'config.php';
        
        if (!key_exists('observers', $config) || !is_array($config['observers']) || empty($config['observers'])) {
        	throw new MonitorException("No se han configurado observadores");
        }
        
        foreach ($config['observers'] as $key => $driverConfig) {
            if (is_string($key)) {
                $driver = $key;
            } elseif (key_exists('driver', $driverConfig)) {
                $driver = $driverConfig['driver'];
            } else {
                throw new MonitorException('No se encontró información para driver');
            }
            // verifico el driver
        	$this->_verifyDriver($driver);
        	// creo la configuración del driver
        	$_config = $this->_getConfig($config, $driverConfig);
        	// creo el driver
        	$class    = "MonitorAWS\\Drivers\\$driver";
        	$observer = new $class($_config);
        	// lo incluyo en los observadores
        	$this->_observers[] = $observer;
        }
    }
    
    /**
     * Llama al patrón observador para observar la métrica de cada uno
     */
    public function watch(){
        foreach ($this->_observers as $observer) {
        	$observer->watch();
        }
    }
    
    /**
     * Verifica si el driver existe como archivo dentro del directorio Drivers.
     * Luego lo carga y espera que exista la clase llamada de la misma manera.
     * Devuelve true si existe el driver o si ya está cargado, devuelve false
     * en caso contrario.
     * 
     * @param string $driver
     * @throws MonitorException
     * @return boolean
     */
    private function _verifyDriver($driver) {
        $fileName = realpath(dirname(__FILE__))."/Drivers/$driver.php";
    	if (!file_exists($fileName)) {
    		throw new MonitorException("No existe archivo para driver: $driver");
    	}
    	$class = "MonitorAWS\\Drivers\\$driver";
    	if (class_exists($class, false)) {
    		return true;
    	} else {
    	    require_once($fileName);
    	    
    	    if (class_exists($class, false)) {
    	        return true;
    	    } else {
    	        throw new MonitorException("No se pudo cargar driver $driver");    	        	
    	    }
    	}
    }
    
    /**
     * Devuelve el array de configuración del driver
     * 
     * @param array $config
     * @param array $driverConfig
     * @return array
     */
    private function _getConfig(array $config, array $driverConfig) {
    	
    	$_config = array(
    		'namespace' => $config['namespace'],
    	    'client'    => $config['client'],
    	);
    	$_config  = $_config + $driverConfig;

    	// ahora veo si le paso el debug
    	if (key_exists('debug', $config) && !key_exists('debug', $driverConfig)) {
    		$_config['debug'] = $config['debug'];    		
    	}

    	if (key_exists('debug_file', $config) && !key_exists('debug_file', $driverConfig)) {
    	    $_config['debug_file'] = $config['debug_file'];
    	}
    	return $_config;
    }
    
}
