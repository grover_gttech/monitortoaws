<?php
namespace MonitorAWS;

/** 
 * Excepción para el monitor de AWS CloudWatch
 * 
 * @author Grover Campos <grover@gttech.pe>
 * @version Mar 31, 2014
 * @copyright GT Tech <http://www.gttech.pe>
 */
class MonitorException extends \Exception
{
    
}
