<?php
namespace MonitorAWS\Drivers;

require_once ('Apache.php');

/**
 * Driver de Apache para monitorear usando Amazon CloudWatch, este driver busca
 * un texto en el html devuelto.
 * 
 * @author Grover Campos <grover@gttech.pe>
 * @version Mar 31, 2014
 * @copyright GT Tech <http://www.gttech.pe>
 */
final class ApacheSubstring extends Apache
{
    private $_substring;
    
    /**
     * Constructor
     * @param array $config
     */
    public function __construct(array $config)
    {
        parent::__construct($config);
        $this->_substring = $config['needle'];
    }
    
    /**
     * (non-PHPdoc) 
     * @see Monitor::watch()
     */
    public function watch() {
    	$watch = $this->_getContent();
    	$valor = (int) !(strstr($watch, $this->_substring) === false);
    	if ($valor == 0) {
    	    $this->error("Error, se obtuvo:\n$watch\n");
    	}
    	$this->publicar($valor, 'Count');
    }
    
}
