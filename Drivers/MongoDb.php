<?php
namespace MonitorAWS\Drivers;
use MonitorAWS\Drivers\DriverAbstract;

/** 
 * Monitor de MongoDb usando AWS CloudWatch
 *
 * @author Grover Campos <grover@gttech.pe>
 * @version Oct 23, 2013
 * @copyright GT Tech <http://www.gttech.pe>
 */
final class MongoDb extends DriverAbstract
{
  protected $_metric = 'MongoDb';
  
  private $_host = '127.0.0.1';
  private $_port = 28017;
  
  public function __construct($config) {
      parent::__construct($config);
      
      if (!empty($config['host'])) {
        $this->_host = $config['host'];
      }
      if (!empty($config['port'])){
          $this->_port = $config['port'];
      }
  }

  /**
   *(non-PHPdoc) 
   * @see Monitor::watch()
   */
  public function watch () {
    $fc = curl_init();
    curl_setopt($fc, CURLOPT_URL,'http://:/serverStatus');
    curl_setopt($fc, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($fc, CURLOPT_HEADER, false);
    curl_setopt($fc, CURLOPT_VERBOSE, false);
    curl_setopt($fc, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($fc, CURLOPT_COOKIESESSION, false);
    curl_setopt($fc, CURLOPT_FRESH_CONNECT, true);
    curl_setopt($fc, CURLOPT_TIMEOUT, 2);
    $watch = curl_exec($fc);
    curl_close($fc);
    
    if ($watch === false) {
      $this->publicar(0, 'Count');
    } else {
      $data = json_decode($watch, true);
      $this->publicar($data['connections']['current'], 'Count');
    }
  }

}