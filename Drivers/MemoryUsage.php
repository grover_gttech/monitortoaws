<?php
namespace MonitorAWS\Drivers;

use MonitorAWS\Drivers\DriverAbstract;

/**
 *
 * Monitorea el porcentaje de memoria usada
 *
 * @author Grover Campos Ancajima <grover@gttech.pe>
 * @version 0.1
 * @copyright GT Tech <http://www.gttech.pe>       
 */
class MemoryUsage extends DriverAbstract
{
    /**
     * Nombre de la métrica, debe ser implementado en cada clase de un driver
     * 
     * @var string
     */
    protected $_metric = 'MemoryUsage';
    
    /**
     * (non-PHPdoc)
     *
     * @see \MonitorAWS\Drivers\DriverAbstract::watch()
     *
     */
    public function watch()
    {
        $output = $ret_var = null;
        $ret = exec('free -t', $output, $ret_var);
        if ($ret_var === 0) {
            $matches = null;
            if (preg_match('/^Mem\:\s+(?P<total>\d+)\s+(?P<used>\d+)\s+(?P<free>\d+).*$/', $output[1], $matches)) {
                
                $percent = $matches['used'] / $matches['total'];
                
                $this->publicar(round($percent * 100), 'Percent');
            } else {
                $this->error('Falló medición de memoria libre');
            }
        } else {
            $this->error('Falló al medir la memoria libre');
        }
    }
}