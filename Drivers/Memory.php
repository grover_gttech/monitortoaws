<?php
namespace MonitorAWS\Drivers;
use MonitorAWS\Drivers\DriverAbstract;

/**
 * Driver para medir estadísticas del uso de memoria de una instancia
 *
 * @author Grover Campos Ancajima <grover@gttech.pe>
 * @version 0.1
 * @copyright GT Tech <http://www.gttech.pe>
 */
class Memory extends DriverAbstract
{
    /**
     * Nombre de la métrica, debe ser implementado en cada clase de un driver
     * 
     * @var string
     */
    protected $_metric = 'Memory';
    
    /**
     * Tipo de memoria a publicar, puede ser:
     * 1 => free
     * 2 => used
     * 3 => total
     * 
     * @var int
     */
    protected $_type = 1;
    /**
     * Arrays con los keys disponibles
     * @var array
     */
    private $_keys = array();
    /**
     * Key a monitorear
     * @var string
     */
    private $_key  = null;
    
    /**
     * Constructor 
     * @param array $config
     */
    public function __construct($config) {
        parent::__construct($config);
        
        $this->_keys = array(
            1 => 'free',
            'used',
            'total'
        );
        
        if (!empty($config['type']) && is_int($config['type']) && key_exists($config['type'], $this->_keys)) {
            $this->_type = $config['type'];
        }
        $this->_key = $this->_keys[$this->_type];
    }

    /**
     * (non-PHPdoc)
     *
     * @see DriverAbstract::watch()
     *
     */
    public function watch()
    {
        $output = $ret_var = $matches = null;
        $regexp = '/^Mem\:\s+(?P<total>\d+)\s+(?P<used>\d+)\s+(?P<free>\d+).*$/';
        
        // consulto al sistema
        $ret = exec('free -t', $output, $ret_var);
        
        if ($ret_var === 0) {
            if (preg_match($regexp, $output[1], $matches)) {
                $mem = ceil($matches[$this->_key] / (1024^2));
                $this->publicar($mem, 'Megabytes');
            } else {
                $this->error('Falló medición de memoria libre');
            }
        } else {
            $this->error('Falló al medir la memoria libre');
        }
    }
}
