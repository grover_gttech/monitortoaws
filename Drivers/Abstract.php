<?php
namespace MonitorAWS\Drivers;

use Aws\CloudWatch\CloudWatchClient;
use Guzzle\Service\Exception\ValidationException;
use MonitorAWS\MonitorException;

/**
 * Clase abstracta de un Monitor usando AWS CloudWatch
 *
 * @author Grover Campos <grover@gttech.pe>
 * @version Oct 23, 2013
 * @copyright GT Tech <http://www.gttech.pe>
 */
abstract class DriverAbstract 
{
  /**
   * Cliente 
   * @var CloudWatchClient
   */
  protected $_client;
  /**
   * 
   * @var string
   */
  protected $_namespace;
  /**
   * Nombre de la métrica, este nombre puede configurarse en el archivo de configuración,
   * de esta manera puede tenerse varios nombres para la misma métrica
   * @var string
   */
  protected $_metric = '';
  /**
   * ¿Muestra con mensajes en consola?
   * @var boolean
   */
  protected $_debug = false;
  /**
   * Nombre del archivo de log
   * @var string
   */
  protected $_debugFile = "Monitor.log";
  
  /**
   * Se instancia cliente hacia CloudWatch
   */
  public function __construct(array $config) {
    $required = array('namespace' => true, 'client' => true);
    // si la clase no especifica el nombre de la métrica 
    if (empty($this->_metric)) {
        // -> debe especificarse obligatoriamente en el archivo de configuración
        $required['metric'] = true;
    } else if(empty($config['metric'])){
        // si la clase la especifica, pero el archivo de configuración no 
        // entonces se le pone el mismo nombre de métrica que el especificado
        // en la clase
        $config['metric'] = $this->_metric;
    } // si se especifica el nombre de la métrica en el archivo de configuración -> siempre se usará ese nombre
    
    $falta    = array_diff_key($required, $config);
    if (!empty($falta)) {
    	throw new MonitorException('Algún parámetro requerido está faltante en la configuración');
    }
    $this->_namespace = $config['namespace'];
    $this->_metric    = $config['metric'];
    $this->_client    = CloudWatchClient::factory($config['client']);
    
    $this->_debug     = key_exists('debug', $config) ? $config['debug'] : false;
    if (key_exists('debug_file', $config)) {
        $this->_debugFile = $config['debug_file'];
    }
  }
  
  /**
   * Publica un valor
   * 
   * @param mixed $valor
   * @param string $unit
   */
  public function publicar($valor, $unit = 'None') {
    try
    {
      $this->log("Se va a publicar desde ".get_class($this).": $valor");
      $this->_client->putMetricData(array(
        'Namespace'  => $this->_namespace,
        'MetricData' => array(
          array(
            'MetricName' => $this->_metric,
            'Timestamp'  => time(),
            'Value'      => $valor,
            'Unit'       => $unit
          )
        )
      ));
    }
    catch (ValidationException $ev)
    {
      // No hago nada porque le jode que publique un 0
      $this->error("Error grave al publicar monitoreo: ".$ev);
    }
  }
  
  /**
   * Método abstracto para que lo implementen los drivers
   */
  abstract public function watch();
  
  /**
   * Envía un mensaje de log al archivo de depuración, en caso que esté activado el debug, sino no hace nada
   * @param string $mensaje
   */
  public function log($mensaje){
    if ($this->_debug){
      $this->error("$mensaje\n");
    }
  }

  /**
   * Envía un mensaje de log al archivo de depuración, no importa la configuración de debug
   * @param string $mensaje
   */
  public function error($mensaje){
    error_log($mensaje, 3, $this->_debugFile);
  }

  /**
   * Devuelve el nombre de la métrica
   * 
   * @return string
   */
  public function getMetricName() {
      return $this->_metric;
  }
  
  /**
   * Devuelve el namespace de la métrica
   * 
   * @return string
   */
  public function getNamespace() {
      return $this->_namespace;
  }
  
}
