<?php
namespace MonitorAWS\Drivers;
use MonitorAWS\Drivers\DriverAbstract;

/** 
 * Monitor de MySQL usando AWS CloudWatch
 *
 * @author Grover Campos <grover@gttech.pe>
 * @version Oct 23, 2013
 * @copyright GT Tech <http://www.gttech.pe>
 */
final class MySQLi extends DriverAbstract
{
  protected $_metric = 'MySQL';
  /**
   * 
   * @var array
   */
  private $_dbConfig = array(
    'host'     => '127.0.0.1',
    'username' => 'test',
    'password' => ''
  );

  /**
   * Constructor
   */
  public function __construct (array $config)
  {
    parent::__construct($config);
    $this->_dbConfig = $config['dbConfig'];
  }

  /**
   *(non-PHPdoc) 
   * @see Monitor::watch()
   */
  public function watch ()
  {
    $host     = $this->_dbConfig['host'];
    $username = $this->_dbConfig['username'];
    $password = $this->_dbConfig['password'];
    $dbname   = null;
    $port     = null;
    $socket   = isset($this->_dbConfig['socket']) ? $this->_dbConfig['socket'] : null;

    /* activate reporting */
    mysqli_report(MYSQLI_REPORT_ALL);

    try {
    
      $cn = new \mysqli($host, $username, $password, $dbname, $port, $socket);
      $rs = $cn->query('SELECT 1 AS `conectado` FROM DUAL');
      $result = (int)($rs instanceof \mysqli_result);
      $rs->close();
      $cn->close();
      
    } catch (\Exception $e) {
      $this->error("Hubo un error al conectarse con MySQL: ".$e->getMessage());
      $result = 0;
    }
  
    $this->publicar($result, 'Count');
        
  }

}
