<?php
namespace MonitorAWS\Drivers;

require_once 'DiskFree.php';
use MonitorAWS\Drivers\DiskFree;

/**
 * Monitorea el porcentaje de espacio usado
 *
 * @author Grover Campos Ancajima <grover@gttech.pe>
 * @version 0.1
 * @copyright GT Tech <http://www.gttech.pe>        
 */
final class DiskUsage extends DiskFree
{
    /**
     * Nombre de la métrica, debe ser implementado en cada clase de un driver
     * 
     * @var string
     */
    protected $_metric = 'DiskUsage';

    /**
     * (non-PHPdoc)
     *
     * @see \MonitorAWS\Drivers\DriverAbstract::watch()
     *
     */
    public function watch()
    {
        $libre = disk_free_space($this->_directorio);
        $total = disk_total_space($this->_directorio);
        
        $percent = ($total - $libre) / $total;
        $this->publicar(round($percent * 100), 'Percent');
    }
}
