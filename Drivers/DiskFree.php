<?php
namespace MonitorAWS\Drivers;
use MonitorAWS\Drivers\DriverAbstract;

/**
 * Driver para monitorear el espacio libre en una partición
 *
 * @author Grover Campos Ancajima <grover@gttech.pe>
 * @version 0.1
 * @copyright GT Tech <http://www.gttech.pe>
 */
class DiskFree extends DriverAbstract
{
    /**
     * Nombre de la métrica, debe ser implementado en cada clase de un driver
     * 
     * @var string
     */
    protected $_metric = 'DiskFree';

    /**
     * Variable de trabajo del driver.
     * Directorio a consultar el espacio libre.
     * 
     * @var string
     */
    protected $_directorio = '/';
    
    /**
     */
    public function __construct(array $config) {
        parent::__construct($config);
        if (!empty($config['directorio'])) {
            $this->_directorio = $config['directorio'];
        }
    }
    
    /**
     * (non-PHPdoc)
     *
     * @see DriverAbstract::watch()
     *
     */
    public function watch() {
        $libre = disk_free_space($this->_directorio) / (1024^2);
        if ($libre) {
            $this->publicar(ceil($libre), 'Megabytes');
        } else {
            $this->error('Falló al buscar el espacio libre');
        }
    }
}
