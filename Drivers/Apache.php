<?php
namespace MonitorAWS\Drivers;
use MonitorAWS\Drivers\DriverAbstract;

/** 
 * Monitor de Apache usando AWS CloudWatch
 *
 * @author Grover Campos <grover@gttech.pe>
 * @version Oct 23, 2013
 * @copyright GT Tech <http://www.gttech.pe>
 */
class Apache extends DriverAbstract
{
  /**
   * URL que se debe verificar
   * 
   * @var string
   */
  private $_url;
  
  protected $_metric = 'Apache';

  /**
   * Constructor
   */
  public function __construct (array $config) {
    parent::__construct($config);
    $this->_url = $config['url'];
  }
  
  /**
   * Devuelve el contenido del url a verificar
   * @return mixed
   */
  protected function _getContent() {
    if (strpos($this->_url,'http://') !== false) {
      $fc = curl_init();
      curl_setopt($fc, CURLOPT_URL,$this->_url);
      curl_setopt($fc, CURLOPT_RETURNTRANSFER, true);
      curl_setopt($fc, CURLOPT_HEADER, false);
      curl_setopt($fc, CURLOPT_VERBOSE, false);
      curl_setopt($fc, CURLOPT_SSL_VERIFYPEER, false);
      curl_setopt($fc, CURLOPT_COOKIESESSION, false);
      curl_setopt($fc, CURLOPT_FRESH_CONNECT, true);
      curl_setopt($fc, CURLOPT_TIMEOUT, 2);
      $watch = curl_exec($fc);
      $info  = curl_getinfo($fc);
      curl_close($fc);
    } else {
      $watch = @file_get_contents($this->_url);
    }
    return $info['http_code'] == 200 ? $watch : false;
  }

  /**
   * (non-PHPdoc) 
   * @see Monitor::watch()
   */
  public function watch () {
    $watch = $this->_getContent();
    $this->publicar($watch === false ? 0 : 1, 'Count');
  }

}
